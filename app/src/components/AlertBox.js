import Component from '../infrastructure/Component'
import { myBus } from '../infrastructure/myBus'

const template = document.createElement('template')

template.innerHTML = `
  <style>
    div { 
      background-color: white;
      color: red;
    }
  </style>
  
  <div></div>
`

export default class AlertBox extends Component {
  constructor() {
    super(template, myBus)
    
    this.bus.subscribe('validation.error', error_message => 
      this.shadowroot.querySelector('div').innerHTML = error_message
    )
  }

}
window.customElements.define('alert-box', AlertBox)