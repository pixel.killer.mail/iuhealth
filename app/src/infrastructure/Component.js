export default class Component extends HTMLElement {
  constructor(template, bus){
    super()
    this.shadowroot = this.attachShadow({ mode: 'open' })
    this.shadowroot.appendChild(template.content.cloneNode(true))
    this.bus = bus
    this.elements = {}
  }

  pickElements(element_selectors = []) {
    element_selectors.forEach(
      selector => this.elements[selector] = this.shadowroot.querySelector(selector)
    )
  }
}